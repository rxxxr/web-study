# 環境構築

以下のソフトウェアを導入してください。

## Node.js

### ソフトウェアのインストール

以下のサイトからLTSの最新版を取得してください。  
https://nodejs.org/ja/

## Visual Studio Code

### ソフトウェアのインストール

以下のサイトから最新版を取得してください。  
https://code.visualstudio.com/

### プラグインのインストール

サイドバーのExtensionsタブを選択して検索欄に `@recommended:workspace` と入力し、表示された拡張機能をインストールする。

# デモ環境

## 環境情報

Firebaseを使用している。  
アカウント：r-toyama@lis.co.jp / ○○

## デプロイ方法

```shell
firebase login
firebase deploy
```
